﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using LoreBuilder.ViewModels;
using ReactiveUI;
using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;

namespace LoreBuilder.Avalonia.Views
{
    public class MainWindow : Window, IViewFor<LoreBuilderViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });

#if DEBUG
            this.AttachDevTools();
#endif
        }

        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (LoreBuilderViewModel)value; }

        public LoreBuilderViewModel ViewModel { get; set; }

        private void PopulateFromViewModel(LoreBuilderViewModel viewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(viewModel,
                vm => vm.Subjects,
                v => v.SubjectsListBox.Items)
                .DisposeWith(disposables);
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public ListBox SubjectsListBox => this.FindControl<ListBox>("SubjectsListBox");
    }
}
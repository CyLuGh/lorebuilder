﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Markup.Xaml;
using Avalonia.ReactiveUI;

namespace LoreBuilder.Avalonia.Views.Lists
{
    public class ActorView : ReactiveUserControl<ActorView>
    {
        public ActorView()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            AvaloniaXamlLoader.Load(this);
        }
    }
}
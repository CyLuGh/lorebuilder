﻿using Avalonia;
using Avalonia.Controls.ApplicationLifetimes;
using Avalonia.Markup.Xaml;
using LoreBuilder.Avalonia.ViewModels;
using LoreBuilder.Avalonia.Views;
using LoreBuilder.ViewModels;

namespace LoreBuilder.Avalonia
{
    public class App : Application
    {
        public override void Initialize()
        {
            AvaloniaXamlLoader.Load(this);
        }

        public override void OnFrameworkInitializationCompleted()
        {
            if (ApplicationLifetime is IClassicDesktopStyleApplicationLifetime desktop)
            {
                desktop.MainWindow = new MainWindow
                {
                    ViewModel = new LoreBuilderViewModel()
                };
            }

            base.OnFrameworkInitializationCompleted();
        }
    }
}
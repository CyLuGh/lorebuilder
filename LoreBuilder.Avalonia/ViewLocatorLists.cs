﻿using Avalonia.Controls;
using Avalonia.Controls.Templates;
using LoreBuilder.Avalonia.Views.Lists;
using LoreBuilder.ViewModels;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Text;

namespace LoreBuilder.Avalonia
{
    public class ViewLocatorLists : IDataTemplate
    {
        public bool SupportsRecycling => false;

        public IControl Build(object data)
        {
            var name = data.GetType().FullName;

            return new TextBlock { Text = "Not Found: " + name };
        }

        public bool Match(object data)
        {
            return data is SubjectViewModel;
        }
    }
}
﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using LoreBuilder.Data;
using LoreBuilder.Data.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace LoreBuilder.ViewModels
{
    public class ItemViewModel : SubjectViewModel
    {
        internal LoreBuilderViewModel LoreBuilderViewModel { get; }

        [Reactive] public string Name { get; set; }
        
        public ReactiveCommand<Unit, Unit> SaveCommand { get; private set; }

        public ItemViewModel(LoreBuilderViewModel loreBuilderViewModel)
        {
            LoreBuilderViewModel = loreBuilderViewModel;

            InitializeCommands(this);
        }

        private void InitializeCommands(ItemViewModel itemViewModel)
        {
            itemViewModel.SaveCommand = ReactiveCommand.CreateFromObservable(() => Observable.Start(() =>
            {
                var clone = (SubjectViewModel)itemViewModel.Clone();
                clone.IsEditing = false;
                clone.LastModificationTime = DateTime.Now;

                var model = Mappings.Mapper.Map<Item>(clone);
                DataManager.Instance.Upsert(model);

                itemViewModel.LoreBuilderViewModel.AddOrUpdate(clone);
            }));
        }

        public override object Clone()
            => new ItemViewModel(LoreBuilderViewModel)
            {
                Id = Id,
                Name = Name,
                CreationTime = CreationTime,
                LastModificationTime = LastModificationTime,
                IsEditing = IsEditing,
                Markdown = Markdown
            };
    }
}
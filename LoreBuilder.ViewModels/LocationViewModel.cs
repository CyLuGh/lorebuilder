﻿using System;
using System.Collections.ObjectModel;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using LoreBuilder.Data;
using LoreBuilder.Data.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace LoreBuilder.ViewModels
{
    public class LocationViewModel : SubjectViewModel
    {
        public LoreBuilderViewModel LoreBuilderViewModel { get; }

        public LocationViewModel(LoreBuilderViewModel loreBuilderViewModel)
        {
            LoreBuilderViewModel = loreBuilderViewModel;

            InitializeCommands(this);

            this.WhenActivated(disposables =>
            {
                LoreBuilderViewModel.SubjectsCache
                                     .Connect()
                                     .Filter(o => o is LocationViewModel lvm && lvm.Id != this.Id)
                                     .Transform(o => (LocationViewModel)o)
                                     .ObserveOn(RxApp.MainThreadScheduler)
                                     .Bind(out _otherLocations)
                                     .DisposeMany()
                                     .Subscribe()
                                     .DisposeWith(disposables);
            });
        }

        [Reactive] public string Name { get; set; }
        [Reactive] public Guid ParentId { get; set; }

        private ReadOnlyObservableCollection<LocationViewModel> _otherLocations;
        public ReadOnlyObservableCollection<LocationViewModel> OtherLocations => _otherLocations;

        public ReactiveCommand<Unit, Unit> SaveCommand { get; private set; }

        private static void InitializeCommands(LocationViewModel locationViewModel)
        {
            locationViewModel.SaveCommand = ReactiveCommand.CreateFromObservable(() => Observable.Start(() =>
            {
                var clone = (SubjectViewModel)locationViewModel.Clone();
                clone.IsEditing = false;
                clone.LastModificationTime = DateTime.Now;

                var model = Mappings.Mapper.Map<Location>(clone);
                DataManager.Instance.Upsert(model);

                locationViewModel.LoreBuilderViewModel.AddOrUpdate(clone);
            }));
            locationViewModel.SaveCommand
                             .ObserveOn(RxApp.MainThreadScheduler)
                             .SubscribeSafe(_ => locationViewModel.IsEditing = false);
        }

        public override object Clone()
        {
            return new LocationViewModel(LoreBuilderViewModel)
            {
                Id = Id,
                Name = Name,
                Markdown = Markdown,
                CreationTime = CreationTime,
                LastModificationTime = LastModificationTime,
                ParentId = ParentId,
                IsEditing = IsEditing
            };
        }
    }
}
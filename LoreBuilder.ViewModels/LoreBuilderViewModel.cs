﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reactive;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Binding;
using LoreBuilder.Data;
using LoreBuilder.Data.Models;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace LoreBuilder.ViewModels
{
    public class LoreBuilderViewModel : ReactiveObject, IActivatableViewModel
    {
        private ReadOnlyObservableCollection<SubjectViewModel> _subjects;
        private ReadOnlyObservableCollection<SceneViewModel> _scenes;

        public LoreBuilderViewModel()
        {
            Mappings.Initialize(this);
            InitializeCommands(this);

            Activator = new ViewModelActivator();

            var filters = this.WhenAnyValue(x => x.ShowCharacters)
                              .CombineLatest(this.WhenAnyValue(x => x.ShowLocations),
                                    this.WhenAnyValue(x=>x.ShowItems),
                                  (sChars, sLocs, sItems) => (Func<SubjectViewModel, bool>)(s =>
                                  {
                                      if (sChars && s is ActorViewModel)
                                          return true;

                                      if (sLocs && s is LocationViewModel)
                                          return true;

                                      if (sItems && s is ItemViewModel)
                                          return true;

                                      return false;
                                  }));

            this.WhenActivated(disposables =>
            {
                SubjectsCache.Connect()
                             .Filter(filters)
                             .Sort(SortExpressionComparer<SubjectViewModel>.Descending(s => s.LastModificationTime))
                             .ObserveOn(RxApp.MainThreadScheduler)
                             .Bind(out _subjects)
                             .DisposeMany()
                             .Subscribe()
                             .DisposeWith(disposables);

                var sceneTree = ScenesCache.Connect()
                    .TransformToTree( s => s.ParentId)
                    .Transform( node => new SceneViewModel(node))
                    .ObserveOn(RxApp.MainThreadScheduler)
                    .Bind(out _scenes)
                    .DisposeMany()
                    .Subscribe()
                    .DisposeWith(disposables);

                this.WhenAnyValue(o => o.SelectedSubject)
                    .Select(o => o?.Clone() as SubjectViewModel)
                    .ToPropertyEx(this, x => x.WorkingSubject)
                    .DisposeWith(disposables);

                Observable.Return(Unit.Default)
                          .InvokeCommand(ReloadDataCommand);
            });
        }

        private static void InitializeCommands(LoreBuilderViewModel loreBuilderViewModel)
        {
            loreBuilderViewModel.ReloadDataCommand = ReactiveCommand.CreateFromObservable(() => Observable.Start(() => loreBuilderViewModel.RestoreData()));

            loreBuilderViewModel.GenerateFakeDataCommand = ReactiveCommand.CreateFromObservable(() => Observable.Start(() => new SampleGenerator().Generate()));
            loreBuilderViewModel.GenerateFakeDataCommand
                                .InvokeCommand(loreBuilderViewModel, x => x.ReloadDataCommand);
        }

        private void RestoreData()
        {
            SubjectsCache.Clear();

            SubjectsCache.AddOrUpdate(
                DataManager.Instance.Get<Actor>()
                    .Select(x => Mappings.Mapper.Map<ActorViewModel>(x))
                );

            SubjectsCache.AddOrUpdate(
               DataManager.Instance.Get<Location>()
                   .Select(x => Mappings.Mapper.Map<LocationViewModel>(x))
               );

            SubjectsCache.AddOrUpdate(
               DataManager.Instance.Get<Item>()
                   .Select(x => Mappings.Mapper.Map<ItemViewModel>(x))
               );

            ScenesCache.AddOrUpdate(
                DataManager.Instance.Get<Scene>()
                    .Select(x => Mappings.Mapper.Map<SceneViewModel>(x))
                );
            
            ScenesCache.AddOrUpdate(
                DataManager.Instance.Get<Act>()
                    .Select(x => Mappings.Mapper.Map<ActViewModel>(x))
                );
        }

        internal SourceCache<SubjectViewModel, Guid> SubjectsCache { get; }
            = new SourceCache<SubjectViewModel, Guid>(o => o.Id);

        internal SourceCache<SceneViewModel,Guid> ScenesCache {get;}
            = new SourceCache<SceneViewModel, Guid>(o => o.Id);

        public ReadOnlyObservableCollection<SubjectViewModel> Subjects => _subjects;
        public ReadOnlyObservableCollection<SceneViewModel> Scenes => _scenes;

        public ReactiveCommand<Unit, Unit> GenerateFakeDataCommand { get; private set; }
        public ReactiveCommand<Unit, Unit> ReloadDataCommand { get; private set; }

        [Reactive] public SubjectViewModel SelectedSubject { get; set; }

        public SubjectViewModel WorkingSubject { [ObservableAsProperty] get; }

        [Reactive] public bool ShowCharacters { get; set; } = true;
        [Reactive] public bool ShowLocations { get; set; } = true;
        [Reactive] public bool ShowItems { get; set; } = true;

        public ViewModelActivator Activator { get; }

        public void AddOrUpdate(SubjectViewModel viewModel)
        {
            SubjectsCache.AddOrUpdate(viewModel);
        }
    }
}
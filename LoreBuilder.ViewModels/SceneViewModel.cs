﻿using System;
using System.Collections.ObjectModel;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using DynamicData;
using DynamicData.Binding;
using ReactiveUI.Fody.Helpers;

namespace LoreBuilder.ViewModels
{
    public class SceneViewModel : SubjectViewModel, IDisposable
    {
        protected readonly IDisposable _cleanUp;

        public LoreBuilderViewModel LoreBuilderViewModel { get; }

        [Reactive] public Guid ParentId { get; set; }

        [Reactive] public bool IsExpanded { get; set; }
        [Reactive] public bool IsSelected { get; set; }
        private ReadOnlyObservableCollection<SceneViewModel> _nested;

        public SceneViewModel(LoreBuilderViewModel loreBuilderViewModel)
        {
            LoreBuilderViewModel = loreBuilderViewModel;
        }

        public SceneViewModel(Node<SceneViewModel,Guid> node)
        {
            LoreBuilderViewModel = node.Item.LoreBuilderViewModel;
            ParentId = node.Item.ParentId;

            // Wrap loader for the nested view model inside a lazy so we can control when it is invoked
            var childrenLoader = new Lazy<IDisposable>(
                    () => node.Children.Connect()
                                        .Transform(e => new SceneViewModel(e))
                                        .Bind(out _nested)
                                        .DisposeMany()
                                        .Subscribe()
                );

            // return true when the children should be loaded 
            // (i.e. if current node is a root, otherwise when the parent expands)
            var shouldExpand = node.IsRoot ?
                Observable.Return(true)
                : node.Parent.Value.Item.WhenValueChanged(x => x.IsExpanded);

            // wire the observable
            var expander = shouldExpand
                    .Where(isExpanded => isExpanded)
                    .Take(1)
                    .Subscribe(_ =>
                    {
                        // force lazy loading
                        var x = childrenLoader.Value;
                    });

            _cleanUp = Disposable.Create(() =>
            {
                expander.Dispose();
                if (childrenLoader.IsValueCreated)
                    childrenLoader.Value.Dispose();
            });
        }

        public override object Clone()
            => new SceneViewModel(LoreBuilderViewModel) {
                Id = Id,
                CreationTime = CreationTime,
                LastModificationTime = LastModificationTime,
                IsEditing = IsEditing,
                Markdown = Markdown
            };

        public void Dispose()
        {
            _cleanUp?.Dispose();
        }
    }
}
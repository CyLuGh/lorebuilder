﻿using AutoMapper;
using LiteDB;
using LoreBuilder.Data.Models;
using LoreBuilder.ViewModels;

namespace LoreBuilder
{
    public static class Mappings
    {
        public static IMapper Mapper { get; private set; }

        public static void Initialize(LoreBuilderViewModel loreBuilderViewModel)
        {
            InitializeDbMapper();
            InitializeModelsMapper(loreBuilderViewModel);
        }

        private static void InitializeDbMapper()
        {
            var mapper = BsonMapper.Global;

            mapper.Entity<Actor>()
                  .Id(x => x.Id);

            mapper.Entity<Location>()
                  .Id(x => x.Id);

            mapper.Entity<Item>()
                  .Id(x => x.Id);

            mapper.Entity<Subject>()
                  .Id(x => x.Id);

            mapper.Entity<Act>()
                  .Id(x => x.Id);
        }

        private static void InitializeModelsMapper(LoreBuilderViewModel loreBuilderViewModel)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Actor, ActorViewModel>()
                   .ForMember(x => x.IsEditing, opt => opt.Ignore())
                   .ForMember(x => x.SaveCommand, opt => opt.Ignore())
                   .ConstructUsing(_ => new ActorViewModel(loreBuilderViewModel))
                   .ReverseMap();

                cfg.CreateMap<Location, LocationViewModel>()
                   .ForMember(x => x.IsEditing, opt => opt.Ignore())
                   .ForMember(x => x.SaveCommand, opt => opt.Ignore())
                   .ForMember(x => x.OtherLocations, opt => opt.Ignore())
                   .ConstructUsing(_ => new LocationViewModel(loreBuilderViewModel))
                   .ReverseMap();

                cfg.CreateMap<Item, ItemViewModel>()
                    .ForMember(x => x.IsEditing, opt => opt.Ignore())
                    .ForMember(x => x.SaveCommand, opt => opt.Ignore())
                    .ConstructUsing(_ => new ItemViewModel(loreBuilderViewModel))
                    .ReverseMap();

                cfg.CreateMap<Act,ActViewModel>()
                    .ForMember(x=>x.IsEditing, opt => opt.Ignore())
                    .ForMember(x=>x.IsExpanded,opt=>opt.Ignore())
                    .ForMember(x=>x.IsSelected,opt=>opt.Ignore())
                    .ConstructUsing(_ => new ActViewModel(loreBuilderViewModel))
                    .ReverseMap();

                cfg.CreateMap<Scene,SceneViewModel>()
                    .ForMember(x=>x.IsEditing, opt => opt.Ignore())
                    .ForMember(x=>x.IsExpanded,opt=>opt.Ignore())
                    .ForMember(x=>x.IsSelected,opt=>opt.Ignore())
                    .ConstructUsing(_ => new SceneViewModel(loreBuilderViewModel))
                    .ReverseMap();
            });

#if DEBUG
            configuration.AssertConfigurationIsValid();
#endif
            Mapper = configuration.CreateMapper();
        }
    }
}
﻿using System;
using System.Reactive;
using System.Reactive.Linq;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;
using LoreBuilder.Data;
using LoreBuilder.Data.Models;

namespace LoreBuilder.ViewModels
{
    public class ActorViewModel : SubjectViewModel
    {
        // TODO: Add aliases
        // TODO: Add relationships

        private readonly LoreBuilderViewModel _loreBuilderViewModel;

        public ActorViewModel(LoreBuilderViewModel loreBuilderViewModel) : base()
        {
            _loreBuilderViewModel = loreBuilderViewModel;
            InitializeCommands(this);

            // this.WhenActivated(disposables =>
            // {
            //     this.WhenAnyValue(x => x.FirstName)
            //         .SubscribeSafe(f => this.Log().Info(f))
            //         .DisposeWith(disposables);
            // });
        }

        [Reactive] public string FirstName { get; set; }
        [Reactive] public string LastName { get; set; }
        [Reactive] public string NickName { get; set; }
        [Reactive] public Gender Gender { get; set; }
        public ReactiveCommand<Unit, Unit> SaveCommand { get; private set; }

        private static void InitializeCommands(ActorViewModel actorViewModel)
        {
            actorViewModel.SaveCommand = ReactiveCommand.CreateFromObservable(() => Observable.Start(() =>
            {
                var clone = (SubjectViewModel)actorViewModel.Clone();
                clone.IsEditing = false;
                clone.LastModificationTime = DateTime.Now;

                var model = Mappings.Mapper.Map<Actor>(clone);
                DataManager.Instance.Upsert(model);

                actorViewModel._loreBuilderViewModel.AddOrUpdate(clone);
            }));
            actorViewModel.SaveCommand
                          .ObserveOn(RxApp.MainThreadScheduler)
                          .SubscribeSafe(_ => actorViewModel.IsEditing = false);
        }

        public override object Clone() =>
            new ActorViewModel(_loreBuilderViewModel)
            {
                Id = Id,
                FirstName = FirstName,
                LastName = LastName,
                NickName = NickName,
                CreationTime = CreationTime,
                LastModificationTime = LastModificationTime,
                IsEditing = IsEditing,
                Markdown = Markdown,
                Gender = Gender
            };
    }
}
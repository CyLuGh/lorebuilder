﻿using System;
using ReactiveUI;
using ReactiveUI.Fody.Helpers;

namespace LoreBuilder.ViewModels
{
    public abstract class SubjectViewModel : ReactiveObject, ICloneable, IActivatableViewModel
    {
        [Reactive] public DateTime CreationTime { get; set; }
        [Reactive] public DateTime LastModificationTime { get; set; }
        [Reactive] public bool IsEditing { get; set; }
        [Reactive] public string Markdown { get; set; } = string.Empty;
        
        public Guid Id { get; protected set; }

        public SubjectViewModel() : this(Guid.NewGuid())
        {
            CreationTime = DateTime.Now;
            LastModificationTime = DateTime.Now;
        }

        public SubjectViewModel(Guid id)
        {
            Activator = new ViewModelActivator();
            Id = id;
        }

        public override int GetHashCode()
            => Id.GetHashCode();

        public abstract object Clone();
        public ViewModelActivator Activator { get; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoreBuilder.ViewModels
{
    public class ActViewModel : SceneViewModel
    {
        public ActViewModel(LoreBuilderViewModel loreBuilderViewModel) : base(loreBuilderViewModel)
        {
        }

        public override object Clone() 
            => new ActViewModel(LoreBuilderViewModel) {
                Id = Id,
                CreationTime = CreationTime,
                LastModificationTime = LastModificationTime,
                IsEditing = IsEditing,
                Markdown = Markdown
            };
    }
}
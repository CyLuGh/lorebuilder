﻿using Bogus;
using LoreBuilder.Data;
using LoreBuilder.Data.Models;
using System;

namespace LoreBuilder
{
    public class SampleGenerator
    {
        public void Generate()
        {
            DataManager.Instance.ClearAll();

            // Randomizer.Seed = new Random(159753);
            Randomizer.Seed = new Random();

            var testActors = new Faker<Actor>()
                .RuleFor(a => a.Gender, f => f.PickRandom<Gender>())
                .RuleFor(a => a.FirstName, f => f.Name.FirstName())
                .RuleFor(a => a.LastName, f => f.Name.LastName())
                .RuleFor(a => a.NickName, f => f.Internet.UserName())
                .RuleFor(a => a.Markdown, f => f.Lorem.Paragraphs())
                .RuleFor(a => a.CreationTime, f => f.Date.Past())
                .RuleFor(a => a.LastModificationTime, f => f.Date.Recent());
            DataManager.Instance.UpsertMany(testActors.Generate(4));

            var testLocations = new Faker<Location>()
                .RuleFor(l => l.Name, f => f.Address.City())
                .RuleFor(l => l.Markdown, f => f.Lorem.Paragraphs(6))
                .RuleFor(l => l.CreationTime, f => f.Date.Past())
                .RuleFor(l => l.LastModificationTime, f => f.Date.Recent());
            DataManager.Instance.UpsertMany(testLocations.Generate(3));

            var testItems = new Faker<Item>()
                .RuleFor(i => i.Name, f => f.Lorem.Word())
                .RuleFor(l => l.Markdown, f => f.Lorem.Paragraphs(6))
                .RuleFor(l => l.CreationTime, f => f.Date.Past())
                .RuleFor(l => l.LastModificationTime, f => f.Date.Recent());
            DataManager.Instance.UpsertMany(testItems.Generate(8));
            
            var testActs = new Faker<Act>()
                .RuleFor(a => a.Markdown, f => f.Lorem.Paragraphs(2))
                .RuleFor(a => a.CreationTime, f => f.Date.Past())
                .RuleFor(a => a.LastModificationTime, f => f.Date.Recent());
            var acts = testActs.Generate(3);
            DataManager.Instance.UpsertMany(acts);

            var testScenes = new Faker<Scene>()
                .RuleFor(s => s.ParentId, f => f.PickRandom(acts).Id)
                .RuleFor(s => s.Markdown, f => f.Lorem.Paragraphs(2))
                .RuleFor(s => s.CreationTime, f => f.Date.Past())
                .RuleFor(s => s.LastModificationTime, f => f.Date.Recent());
            var scenes = testScenes.Generate(6);
            DataManager.Instance.UpsertMany(scenes);

            testScenes = new Faker<Scene>()
                .RuleFor(s => s.ParentId, f => f.PickRandom(scenes).Id)
                .RuleFor(s => s.Markdown, f => f.Lorem.Paragraphs(2))
                .RuleFor(s => s.CreationTime, f => f.Date.Past())
                .RuleFor(s => s.LastModificationTime, f => f.Date.Recent());
            DataManager.Instance.UpsertMany(testScenes.Generate(8));
        }
    }
}
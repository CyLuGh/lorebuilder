﻿using System;
using System.Collections.Generic;

namespace LoreBuilder.Data.Models
{
    public class Location : Subject
    {
        public string Name { get; set; }

        public Guid ParentId { get; set; }
    }
}
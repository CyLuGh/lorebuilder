﻿using System;

namespace LoreBuilder.Data.Models
{
    public enum Gender
    {
        Undefined = 0,
        Male = 1,
        Female = 2,
        Transgender = 3,
        Other = 4,
        None = 5
    }

    public class Actor : Subject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NickName { get; set; }
        public Gender Gender { get; set; }
    }
}
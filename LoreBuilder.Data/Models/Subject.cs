﻿using System;

namespace LoreBuilder.Data.Models
{
    public abstract class Subject
    {
        public Guid Id { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime LastModificationTime { get; set; }
        public string Markdown { get; set; }
    }
}
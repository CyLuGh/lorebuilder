﻿using LiteDB;
using LoreBuilder.Data.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace LoreBuilder.Data
{
    public class DataManager
    {
        public static DataManager Instance { get; } = new DataManager();

        public string DbFile { get; set; }

        private DataManager()
        {
            DbFile = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "loreBuilder.db");
        }

        public void ClearAll()
        {
            using var db = new LiteDatabase(DbFile);
            db.DropCollection(typeof(Actor).Name);
            db.DropCollection(typeof(Location).Name);
            db.DropCollection(typeof(Item).Name);
            db.DropCollection(typeof(Act).Name);
            db.DropCollection(typeof(Scene).Name);
        }

        public List<T> Get<T>()
        {
            using var db = new LiteDatabase(DbFile);
            var data = db.GetCollection<T>(typeof(T).Name);
            return data.FindAll().ToList();
        }

        public void Upsert<T>(T input) 
        {
            using var db = new LiteDatabase(DbFile);
            var data = db.GetCollection<T>(typeof(T).Name);
            data.Upsert(input);
        }

        public void UpsertMany<T>(IEnumerable<T> inputs)
        {
            using var db = new LiteDatabase(DbFile);
            var data = db.GetCollection<T>(typeof(T).Name);
            data.Upsert(inputs);
        }
    }
}
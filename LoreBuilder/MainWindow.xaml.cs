﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using LoreBuilder.ViewModels;
using ReactiveUI;

namespace LoreBuilder
{
    public partial class MainWindow : IViewFor<LoreBuilderViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });

            ViewModel = new LoreBuilderViewModel();
        }

        private void PopulateFromViewModel(LoreBuilderViewModel viewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(viewModel,
                    vm => vm.Subjects,
                    v => v.EntitiesListBox.ItemsSource)
                .DisposeWith(disposables);

            this.OneWayBind(viewModel,
                    vm => vm.Scenes,
                    v => v.TreeViewScenes.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(viewModel,
                    vm => vm.SelectedSubject,
                    v => v.EntitiesListBox.SelectedItem)
                .DisposeWith(disposables);

            this.Bind(viewModel,
                    vm => vm.WorkingSubject,
                    v => v.ViewHostEditor.ViewModel)
                .DisposeWith(disposables);

            this.Bind(viewModel,
                    vm => vm.ShowCharacters,
                    v => v.CheckBoxFilterCharacters.IsChecked)
                .DisposeWith(disposables);

            this.Bind(viewModel,
                    vm => vm.ShowLocations,
                    v => v.CheckBoxFilterLocations.IsChecked)
                .DisposeWith(disposables);

            this.Bind(viewModel,
                    vm => vm.ShowItems,
                    v => v.CheckBoxFilterItems.IsChecked)
                .DisposeWith(disposables);

            this.BindCommand(viewModel,
                    vm => vm.GenerateFakeDataCommand,
                    v => v.ButtonGenerate)
                .DisposeWith(disposables);
        }

        object IViewFor.ViewModel { get => ViewModel; set => ViewModel = (LoreBuilderViewModel)value; }

        public LoreBuilderViewModel ViewModel { get; set; }
    }
}
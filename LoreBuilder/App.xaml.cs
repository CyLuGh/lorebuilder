﻿using System.Globalization;
using System.Windows;
using System.Windows.Markup;
using LoreBuilder.ViewModels;
using ReactiveUI;
using Splat;
using Splat.NLog;

namespace LoreBuilder
{
    public partial class App
    {
        public App()
        {
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));
            FrameworkElement.LanguageProperty.OverrideMetadata(typeof(System.Windows.Documents.TextElement), new FrameworkPropertyMetadata(XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

            ConfigureLogs();
            Locator.CurrentMutable.UseNLogWithWrappingFullLogger();

            Locator.CurrentMutable.Register(() => new Views.Lists.ActorView(), typeof(IViewFor<ActorViewModel>), "List");
            Locator.CurrentMutable.Register(() => new Views.Editors.ActorView(), typeof(IViewFor<ActorViewModel>), "Editor");

            Locator.CurrentMutable.Register(() => new Views.Lists.LocationView(), typeof(IViewFor<LocationViewModel>), "List");
            Locator.CurrentMutable.Register(() => new Views.Editors.LocationView(), typeof(IViewFor<LocationViewModel>), "Editor");

            Locator.CurrentMutable.Register(() => new Views.Lists.ItemView(), typeof(IViewFor<ItemViewModel>), "List");
            Locator.CurrentMutable.Register(() => new Views.Editors.ItemView(), typeof(IViewFor<ItemViewModel>), "Editor");
        }

        private static void ConfigureLogs()
        {
            var config = new NLog.Config.LoggingConfiguration();

            // Targets where to log to: File and Console
            var logfile = new NLog.Targets.FileTarget("logfile")
            {
                FileName = "LoreBuilder.log",
                DeleteOldFileOnStartup = true
            };
            var logconsole = new NLog.Targets.ConsoleTarget("logconsole");

            // Rules for mapping loggers to targets
            config.AddRule(NLog.LogLevel.Info, NLog.LogLevel.Fatal, logconsole);
            config.AddRule(NLog.LogLevel.Debug, NLog.LogLevel.Fatal, logfile);

            // Apply config
            NLog.LogManager.Configuration = config;
        }
    }
}
﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using LoreBuilder.ViewModels;
using ReactiveUI;

namespace LoreBuilder.Views.Editors
{
    public partial class ItemView
    {
        public ItemView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        public void PopulateFromViewModel(ItemViewModel itemViewModel, CompositeDisposable disposables)
        {
            this.Bind(itemViewModel,
                vm => vm.Name,
                v => v.TextBoxName.Text)
                .DisposeWith(disposables);

             this.Bind(itemViewModel,
                    vm => vm.Markdown,
                    v => v.TextEditor.Document.Text)
                .DisposeWith(disposables);

            this.Bind(itemViewModel,
                    vm => vm.Markdown,
                    v => v.MarkdownViewer.Markdown)
                .DisposeWith(disposables);

            this.OneWayBind(itemViewModel,
                    vm => vm.IsEditing,
                    v => v.MarkdownViewer.Visibility,
                    editing => editing ? Visibility.Collapsed : Visibility.Visible)
                .DisposeWith(disposables);

            this.OneWayBind(itemViewModel,
                    vm => vm.IsEditing,
                    v => v.TextEditor.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.Bind(itemViewModel,
                    vm => vm.IsEditing,
                    v => v.ToggleButtonEditing.IsChecked)
                .DisposeWith(disposables);

            this.OneWayBind(itemViewModel,
                    vm => vm.IsEditing,
                    v => v.UniformGridTools.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.OneWayBind(itemViewModel,
                    vm => vm.IsEditing,
                    v => v.TextBoxName.IsReadOnly,
                    editing => !editing)
                .DisposeWith(disposables);

            this.OneWayBind(itemViewModel,
                vm => vm.CreationTime,
                v => v.TextBoxCreationTime.Text,
                t => t.ToString("yyyy-MM-dd HH:mm:ss"))
            .DisposeWith(disposables);

            this.OneWayBind(itemViewModel,
                vm => vm.LastModificationTime,
                v => v.TextBoxLastModificationTime.Text,
                t => t.ToString("yyyy-MM-dd HH:mm:ss"))
            .DisposeWith(disposables);

             this.BindCommand(itemViewModel,
                    vm => vm.SaveCommand,
                    v => v.ButtonSave)
                .DisposeWith(disposables);
        }
    }
}
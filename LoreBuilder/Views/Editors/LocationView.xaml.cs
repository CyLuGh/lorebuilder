﻿using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using LoreBuilder.ViewModels;
using ReactiveUI;

namespace LoreBuilder.Views.Editors
{
    public partial class LocationView
    {
        public LocationView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(LocationViewModel locationViewModel, CompositeDisposable disposables)
        {
            this.Bind(locationViewModel,
                    vm => vm.Name,
                    v => v.TextBoxName.Text)
                .DisposeWith(disposables);

            this.Bind(locationViewModel,
                    vm => vm.Markdown,
                    v => v.TextEditor.Document.Text)
                .DisposeWith(disposables);

            this.Bind(locationViewModel,
                    vm => vm.Markdown,
                    v => v.MarkdownViewer.Markdown)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.OtherLocations,
                    v => v.ComboBoxParent.ItemsSource)
                .DisposeWith(disposables);

            this.Bind(locationViewModel,
                    vm => vm.ParentId,
                    v => v.ComboBoxParent.SelectedItem,
                    guid => locationViewModel.LoreBuilderViewModel.Subjects?.FirstOrDefault(o => o.Id == guid),
                    o => o != null && o is LocationViewModel lvm ? lvm.Id : Guid.Empty)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.MarkdownViewer.Visibility,
                    editing => editing ? Visibility.Collapsed : Visibility.Visible)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.TextEditor.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.Bind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.ToggleButtonEditing.IsChecked)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.UniformGridTools.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.TextBoxName.IsReadOnly,
                    editing => !editing)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.ComboBoxParent.IsHitTestVisible)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.IsEditing,
                    v => v.ComboBoxParent.Focusable)
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.LastModificationTime,
                    v => v.TextBoxLastModificationTime.Text,
                    t => t.ToString("yyyy-MM-dd HH:mm:ss"))
                .DisposeWith(disposables);

            this.OneWayBind(locationViewModel,
                    vm => vm.CreationTime,
                    v => v.TextBoxCreationTime.Text,
                    t => t.ToString("yyyy-MM-dd HH:mm:ss"))
                .DisposeWith(disposables);

            this.BindCommand(locationViewModel,
                    vm => vm.SaveCommand,
                    v => v.ButtonSave)
                .DisposeWith(disposables);
        }
    }
}
﻿using System;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows;
using LoreBuilder.Data.Models;
using LoreBuilder.ViewModels;
using MaterialDesignThemes.Wpf;
using ReactiveUI;

namespace LoreBuilder.Views.Editors
{
    public partial class ActorView
    {
        public ActorView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x =>
                    {
                        x.Activator.Activate().DisposeWith(disposables);
                        PopulateFromViewModel(x, disposables);
                    })
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(ActorViewModel actorViewModel, CompositeDisposable disposables)
        {
            ComboBoxSex.ItemsSource = Enum.GetValues(typeof(Gender)).OfType<Gender>();

            this.Bind(actorViewModel,
                    vm => vm.LastName,
                    v => v.TextBoxLastName.Text)
                .DisposeWith(disposables);

            this.Bind(actorViewModel,
                    vm => vm.FirstName,
                    v => v.TextBoxFirstName.Text)
                .DisposeWith(disposables);

            this.Bind(actorViewModel,
                    vm => vm.NickName,
                    v => v.TextBoxNickName.Text)
                .DisposeWith(disposables);

            this.Bind(actorViewModel,
                    vm => vm.Markdown,
                    v => v.TextEditor.Document.Text)
                .DisposeWith(disposables);

            this.Bind(actorViewModel,
                    vm => vm.Markdown,
                    v => v.MarkdownViewer.Markdown)
                .DisposeWith(disposables);

            this.Bind(actorViewModel,
                    vm => vm.Gender,
                    v => v.ComboBoxSex.SelectedItem)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.Gender,
                    v => v.PackIconSex.Kind,
                    s => s switch
                    {
                        Gender.Male => PackIconKind.GenderMale,
                        Gender.Female => PackIconKind.GenderFemale,
                        Gender.Transgender => PackIconKind.GenderTransgender,
                        Gender.Other => PackIconKind.GenderMaleFemaleVariant,
                        Gender.None => PackIconKind.None,
                        Gender.Undefined => PackIconKind.GenderEnby,
                        _ => PackIconKind.QuestionMark
                    })
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.MarkdownViewer.Visibility,
                    editing => editing ? Visibility.Collapsed : Visibility.Visible)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.TextEditor.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.Bind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.ToggleButtonEditing.IsChecked)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.TextBoxLastName.IsReadOnly,
                    editing => !editing)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.TextBoxFirstName.IsReadOnly,
                    editing => !editing)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.TextBoxNickName.IsReadOnly,
                    editing => !editing)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.ComboBoxSex.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.PackIconSex.Visibility,
                    editing => editing ? Visibility.Collapsed : Visibility.Visible)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.IsEditing,
                    v => v.UniformGridTools.Visibility,
                    editing => editing ? Visibility.Visible : Visibility.Collapsed)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.LastModificationTime,
                    v => v.TextBoxLastModificationTime.Text,
                    t => t.ToString("yyyy-MM-dd HH:mm:ss"))
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.CreationTime,
                    v => v.TextBoxCreationTime.Text,
                    t => t.ToString("yyyy-MM-dd HH:mm:ss"))
                .DisposeWith(disposables);

            this.BindCommand(actorViewModel,
                    vm => vm.SaveCommand,
                    v => v.ButtonSave)
                .DisposeWith(disposables);
        }
    }
}
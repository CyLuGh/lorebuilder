﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using LoreBuilder.ViewModels;
using ReactiveUI;

namespace LoreBuilder.Views.Lists
{
    public partial class ActorView
    {
        public ActorView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(ActorViewModel actorViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(actorViewModel,
                    vm => vm.LastName,
                    v => v.TextBlockLastName.Text)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.FirstName,
                    v => v.TextBlockFirstName.Text)
                .DisposeWith(disposables);

            this.OneWayBind(actorViewModel,
                    vm => vm.NickName,
                    v => v.TextBlockNickName.Text)
                .DisposeWith(disposables);
        }
    }
}
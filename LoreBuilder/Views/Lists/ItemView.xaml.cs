﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using LoreBuilder.ViewModels;
using ReactiveUI;

namespace LoreBuilder.Views.Lists
{
    public partial class ItemView
    {
        public ItemView()
        {
            InitializeComponent();

            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(ItemViewModel itemViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(itemViewModel,
                  vm => vm.Name,
                  v => v.TextBlockName.Text)
              .DisposeWith(disposables);
        }
    }
}
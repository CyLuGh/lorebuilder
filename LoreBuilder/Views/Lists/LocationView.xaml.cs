﻿using System;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Windows.Controls;
using LoreBuilder.ViewModels;
using ReactiveUI;

namespace LoreBuilder.Views.Lists
{
    public partial class LocationView 
    {
        public LocationView()
        {
            InitializeComponent();
            
            this.WhenActivated(disposables =>
            {
                this.WhenAnyValue(v => v.ViewModel)
                    .Where(vm => vm != null)
                    .Do(x => PopulateFromViewModel(x, disposables))
                    .Subscribe()
                    .DisposeWith(disposables);
            });
        }

        private void PopulateFromViewModel(LocationViewModel locationViewModel, CompositeDisposable disposables)
        {
            this.OneWayBind(locationViewModel,
                    vm => vm.Name,
                    v => v.TextBlockName.Text)
                .DisposeWith(disposables);
        }
    }
}